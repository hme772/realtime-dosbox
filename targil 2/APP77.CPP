/********************************************************************/
/* A Small Real Time System for the Real-Time laboratory            */
/* built by: A.Teitelbaum on an idea of H.G.Mendelbaum              */
/* Jerusalem College of Technology, 5759-64 (1999-03)               */
/* update  Tishrey   5777                                           */
/* APP77.CPP, an application to demonstrate SMARTS77  functioning   */
/********************************************************************/

/*By:
 הודיה אלימלך 318627247

*/

#include "smarts77.h"
Mutex mO = Mutex(0);//the output mutex doesn't need priority inheritance
//1=with 0=without
Mutex m1 = Mutex(0);
Mutex m2 = Mutex(0);
Event evA, evB, evD, evE;
// user Functions
void a()
{
	mO.acquire();
	FILE *f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************   A Start    *********************");
	cout << "\n *************   A Start    *********************";
	fclose(f);
	mO.release();
	int j = 0;
	long i = 0;
	for (j = 0; j < 15; j++)
	{
		for (i = 0; i < 200000; i++);
		mO.acquire();
		f = fopen("t2.txt", "a");  //a  == append
		fprintf(f, "A");
		cout << "A";
		fclose(f);
		mO.release();
	}
	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************   evA Wait    *********************");
	cout << "\n *************   evA Wait    *********************";
	fclose(f);
	mO.release();

	char c = 'A';
	evA.wait(c);
	mO.acquire();
//	cout << "\n I am here after wait in A";
	mO.release();

	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************   evA accept event    *********************");
	cout << "\n *************   evA accept event    *********************";
	mO.release();
	fclose(f);
	for (j = 0; j < 15; j++)
	{
		for (i = 0; i < 200000; i++);
		mO.acquire();
		f = fopen("t2.txt", "a");  //a  == append
		fprintf(f, "A");
		cout << "A";
		fclose(f);
		mO.release();
	}
	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************  A m1 acquire Start    *********************");
	cout << "\n ************* A  m1 acquire Start    *********************";
	fclose(f);
	mO.release();

	m1.acquire();

	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************  A m1 acquire Finish    *********************");
	cout << "\n ************* A  m1 acquire Finish    *********************";
	fclose(f);
	mO.release();

	for (j = 0; j < 15; j++)
	{
		for (i = 0; i < 200000; i++);
		mO.acquire();
		f = fopen("t2.txt", "a");  //a  == append
		fprintf(f, "A");
		cout << "A";
		fclose(f);
		mO.release();
	}
	m1.release();

	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************   A Finish    *********************");
	cout << "\n *************   A Finish   *********************";
	fclose(f);
	mO.release();
}

void b()
{
	mO.acquire();
	FILE *f = fopen("t2.txt", "a");  //a  == append

	fprintf(f, "\n *************   B Start    *********************");
	cout << "\n *************   B Start    *********************";
	fclose(f);
	mO.release();

	int j = 0;
	long i = 0;
	for (j = 0; j < 15; j++)
	{
		for (i = 0; i < 200000; i++);
		mO.acquire();
		f = fopen("t2.txt", "a");  //a  == append
		fprintf(f, "B");
		cout << "B";
		fclose(f);
		mO.release();
	}
	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************   evB Wait    *********************");
	cout << "\n *************   evB Wait    *********************";
	fclose(f);
	mO.release();

	char c = 'B';
	evB.wait(c);


	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************   evB accept event    *********************");
	cout << "\n *************   evB accept event    *********************";
	fclose(f);
	mO.release();

	for (j = 0; j < 15; j++)
	{
		for (i = 0; i < 200000; i++);
		mO.acquire();
		f = fopen("t2.txt", "a");  //a  == append
		fprintf(f, "B");
		cout << "B";
		fclose(f);
		mO.release();
	}

	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************   B Finish    *********************");
	cout << "\n *************   B Finish   *********************";
	fclose(f);
	mO.release();
}
void c()
{
	mO.acquire();
	FILE *f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************   C Start    *********************");
	cout << "\n *************   C Start    *********************";
	fclose(f);
	mO.release();

	int j = 0;
	long i = 0;
	for (j = 0; j < 15; j++)
	{
		for (i = 0; i < 200000; i++);
		mO.acquire();
		f = fopen("t2.txt", "a");  //a  == append
		fprintf(f, "C");
		cout << "C";
		fclose(f);
		mO.release();
	}

	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************  C m1 acquire     *********************");
	cout << "\n ************* C  m1 acquire     *********************";
	fclose(f);
	mO.release();

	m1.acquire();

	for (j = 0; j < 15; j++)
	{
		for (i = 0; i < 200000; i++);
		m1.acquire();
		f = fopen("t2.txt", "a");  //a  == append
		fprintf(f, "C");
		cout << "C";
		fclose(f);
		m1.release();
	}
	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************  C evA send event    *********************");
	cout << "\n ************* C evA send event     *********************";
	fclose(f);
	mO.release();

	evA.send('A', NULL, 0);

	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n ************* C  evB send event    *********************");
	cout << "\n ************* C evB send event     *********************";
	fclose(f);
	mO.release();


	evB.send('B', NULL, 0);


	for (j = 0; j < 15; j++)
	{
		for (i = 0; i < 200000; i++);
		mO.acquire();
		f = fopen("t2.txt", "a");  //a  == append
		fprintf(f, "C");
		cout << "C";
		fclose(f);
		mO.release();
	}

	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************  C m1 release Start    *********************");
	cout << "\n ************* C  m1 release Start    *********************";
	fclose(f);
	mO.release();

	m1.release();

	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n ************* C  m1 release Finish    *********************");
	cout << "\n ************* C  m1 release Finish    *********************";
	fclose(f);
	mO.release();

	for (j = 0; j < 15; j++)
	{
		for (i = 0; i < 200000; i++);
		mO.acquire();
		f = fopen("t2.txt", "a");  //a  == append
		fprintf(f, "C");
		cout << "C";
		fclose(f);
		mO.release();
	}
	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************   C Finish    *********************");
	cout << "\n *************   C Finish   *********************";
	fclose(f);
	mO.release();



}





void d()
{
	mO.acquire();
	FILE *f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************   D Start    *********************");
	cout << "\n *************   D Start    *********************";
	fclose(f);
	mO.release();
	int j = 0;
	long i = 0;
	for (j = 0; j < 15; j++)
	{
		for (i = 0; i < 200000; i++);
		mO.acquire();
		f = fopen("t2.txt", "a");  //a  == append
		fprintf(f, "D");
		cout << "D";
		fclose(f);
		mO.release();
	}
	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************   evD Wait    *********************");
	cout << "\n *************   evD Wait    *********************";
	fclose(f);
	mO.release();

	char c = 'D';
	evD.wait(c);
	mO.acquire();
	//	cout << "\n I am here after wait in A";
	mO.release();

	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************   evD accept event    *********************");
	cout << "\n *************   evD accept event    *********************";
	mO.release();
	fclose(f);
	for (j = 0; j < 15; j++)
	{
		for (i = 0; i < 200000; i++);
		mO.acquire();
		f = fopen("t2.txt", "a");  //a  == append
		fprintf(f, "D");
		cout << "D";
		fclose(f);
		mO.release();
	}
	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************  D m2 acquire Start    *********************");
	cout << "\n ************* D  m2 acquire Start    *********************";
	fclose(f);
	mO.release();

	m2.acquire();

	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************  D m2 acquire Finish    *********************");
	cout << "\n ************* D  m2 acquire Finish    *********************";
	fclose(f);
	mO.release();

	for (j = 0; j < 15; j++)
	{
		for (i = 0; i < 200000; i++);
		mO.acquire();
		f = fopen("t2.txt", "a");  //a  == append
		fprintf(f, "D");
		cout << "D";
		fclose(f);
		mO.release();
	}

	m2.release();

	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************   D Finish    *********************");
	cout << "\n *************   D Finish   *********************";
	fclose(f);
	mO.release();
}

void e()
{
	mO.acquire();
	FILE *f = fopen("t2.txt", "a");  //a  == append

	fprintf(f, "\n *************   E Start    *********************");
	cout << "\n *************   E Start    *********************";
	fclose(f);
	mO.release();

	int j = 0;
	long i = 0;
	for (j = 0; j < 15; j++)
	{
		for (i = 0; i < 200000; i++);
		mO.acquire();
		f = fopen("t2.txt", "a");  //a  == append
		fprintf(f, "E");
		cout << "E";
		fclose(f);
		mO.release();
	}
	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************   evE Wait    *********************");
	cout << "\n *************   evE Wait    *********************";
	fclose(f);
	mO.release();

	char c = 'E';
	evE.wait(c);


	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************   evE accept event    *********************");
	cout << "\n *************   evE accept event    *********************";
	fclose(f);
	mO.release();

	for (j = 0; j < 15; j++)
	{
		for (i = 0; i < 200000; i++);
		mO.acquire();
		f = fopen("t2.txt", "a");  //a  == append
		fprintf(f, "E");
		cout << "E";
		fclose(f);
		mO.release();
	}

	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************   E Finish    *********************");
	cout << "\n *************   E Finish   *********************";
	fclose(f);
	mO.release();
}


void f()
{
	mO.acquire();
	FILE *f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************  F  Start    *********************");
	cout << "\n *************   F Start    *********************";
	fclose(f);
	mO.release();

	int j = 0;
	long i = 0;
	for (j = 0; j < 15; j++)
	{
		for (i = 0; i < 200000; i++);
		mO.acquire();
		f = fopen("t2.txt", "a");  //a  == append
		fprintf(f, "F");
		cout << "F";
		fclose(f);
		mO.release();
	}

	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************  F m2 acquire     *********************");
	cout << "\n ************* F  m2 acquire     *********************";
	fclose(f);
	mO.release();

	m2.acquire();

	for (j = 0; j < 15; j++)
	{
		for (i = 0; i < 200000; i++);
		mO.acquire();
		f = fopen("t2.txt", "a");  //a  == append
		fprintf(f, "F");
		cout << "F";
		fclose(f);
		mO.release();
	}
	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************  F evD send event    *********************");
	cout << "\n ************* F evD send event     *********************";
	fclose(f);
	mO.release();

	evD.send('D', NULL, 0);

	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n ************* F  evF send event    *********************");
	cout << "\n ************* F evF send event     *********************";
	fclose(f);
	mO.release();


	evE.send('E', NULL, 0);


	for (j = 0; j < 15; j++)
	{
		for (i = 0; i < 200000; i++);
		mO.acquire();
		f = fopen("t2.txt", "a");  //a  == append
		fprintf(f, "F");
		cout << "F";
		fclose(f);
		mO.release();
	}

	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************  F m2 release Start    *********************");
	cout << "\n ************* F  m2 release Start    *********************";
	fclose(f);
	mO.release();

	m2.release();

	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n ************* F  m2 release Finish    *********************");
	cout << "\n ************* F  m2 release Finish    *********************";
	fclose(f);
	mO.release();

	for (j = 0; j < 15; j++)
	{
		for (i = 0; i < 200000; i++);
		mO.acquire();
		f = fopen("t2.txt", "a");  //a  == append
		fprintf(f, "F");
		cout << "F";
		fclose(f);
		mO.release();
	}
	mO.acquire();
	f = fopen("t2.txt", "a");  //a  == append
	fprintf(f, "\n *************   F Finish    *********************");
	cout << "\n *************   F Finish   *********************";
	fclose(f);
	mO.release();



}




void main( )
{
	clrscr();
	SMARTS.externalFunctions(timerInterruptHandler, scheduler, myTaskEnd, RMS ); //roundRobin EDF
	SMARTS.declareTask(a,'A',4,400);
	SMARTS.declareTask(b,'B',4,401);
	SMARTS.declareTask(c,'C',4,402);


	SMARTS.declareTask(d, 'D',4 ,400);
	SMARTS.declareTask(e, 'E', 4, 401);
	SMARTS.declareTask(f, 'F', 4, 402);

	SMARTS.runTheTasks();
}
//50, 70, 150 stay as is

// j is the number of times the loop runs in every task:

// if j<=22 both EDF and RR work

// if 23 =< j <= 26, EDF works but RR doesn't work

// if j >= 27 both EDF and RR don't work



