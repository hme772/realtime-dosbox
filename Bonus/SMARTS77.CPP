/****************************************************************/
/* A Small Real Time System for the Real-Time laboratory        */
/* built by: A.Teitelbaum on an idea of H.G.Mendelbaum          */
/* Jerusalem College of Technology, 5759-64 (1999)              */
/* update  Tishrey   5777                                       */
/* SMARTS77.CPP, SMARTS class body                              */
/****************************************************************/
#include "smarts77.h"

/*By:
 הודיה אלימלך 318627247
*/

/**********    Function     **********/
unsigned getTimerClocks()
// Gets the remaining clocks of the timer register
{
	unsigned clocks;
	/* latch counter #0 */
	outportb(0x43, 0x00);
	/* read counter #0 low byte */
	clocks = inportb(0x40);
	/* read counter #0 high byte */
	clocks += inportb(0x40) << 8;
	return clocks;
}
////////////////////////////////////////////////////
/**********    class body:  Parallelism     **********/
Parallelism::Parallelism()
{
	currentTask = 0;
	sleepTasks = 0;
	activeTasks = 0;
	totalTasks = 0;
	deadlock = false;
	error = 0; //----------------------------NEW
//	contextSwitchFlag = true;
	endOfTimeSlice = true;
}

void Parallelism::externalFunctions(void interrupt(*timerInterruptHandler)(...),
	void far *scheduler, void far *userTaskEnd,
	int far(*algorithm)())
	// Sets the external functions
{
	this->timerInterruptHandler = timerInterruptHandler;
	this->scheduler = scheduler;
	this->userTaskEnd = userTaskEnd;
	this->algorithm = algorithm;
	contextSched.declare(scheduler, userTaskEnd, 'S', 0, 0); // prepare the stack of the scheduler task
	for (int i = MaxStack - 1; i >= (MaxStack - 14); i--)
		schedCopy[i] = contextSched.stack[i];
}

int Parallelism::declareTask(void far *code, char name, /*NEW*/int numOfCyclesLeft, int cycleTime /*NEW*/)
// Insert a new task entry in SMARTS context array [ ]
{
	if (totalTasks < MaxTask - 1)
	{
		context[totalTasks++].declare(code, userTaskEnd, name, /*I add*/ numOfCyclesLeft, cycleTime/*I add*/);
		++activeTasks;
		return true;
	}
	else
		return false;
}

void Parallelism::runTheTasks()
// Start running all tasks declared in SMARTS.		
{
	context[totalTasks].status = READY;	//for task 'runTheTasks' (IDLE task)
	context[totalTasks].priority = MAXINT;
	context[totalTasks].currentPriority = MAXINT;

	currentTask = totalTasks;

	asm	cli;		// forbids interrupts (clear interrupts) while changing the interrupt vect
		// saves the original BIOS userInt in our variable 'userIntAddress' to be restored at the end
	userIntAddress = getvect(userInt);	// BIOS userInt 0x60  (unused by PC)
				// puts the normal BIOS timerInt into the unused userInt address
	setvect(userInt, getvect(timerInt));	// timerInt 0x08 

	// sets our SMARTS external function 'timerInterruptHandler' as the new PC hard interrupt time handler
	setvect(timerInt, timerInterruptHandler);
	asm	sti;	// allows back interrupts (set interrupts)

		// waits for end of runTheTasks (end of all the tasks)
	while (true)
	{
		if (deadlock)
		{
			textcolor(RED);
			cprintf("\n\n\rExit : deadlock");
			break;
		}
		//------------------- NEW -------------------------
		if (error)
		{
			textcolor(RED);
			cprintf("\n\n\rExit : Error occur");
			break;
		}
		//------------------- NEW -------------------------


		//before: if (activeTasks==0)
		if (allTaskFinished())//------------------- NEW -------------------------
		{
			cprintf("\n\n\rExit : finish");
			break;
		}
	}

	// restore the original BIOS 'interrupt vector' at the end before returning to regular DOS
	asm	cli;	// no interrupts
	setvect(timerInt, getvect(userInt));	// restore original BIOS time handler
	setvect(userInt, userIntAddress);	// restore original BIOS userInt
	asm	sti;	// yes interrupts
}

//----------------------------------------------- NEW
// checking if all the tasks were finished
int Parallelism::allTaskFinished()
{
	for (int i = 0; i < totalTasks; ++i)
	{
		if ((getNumOfCyclesLeft(i) != 0))
		{
			return 0;
		}
	}
	return 1;
}



void Parallelism::callScheduler()
// Return the control to the scheduler, this sets ON the software interrupt ProgInt flag
{
	setProgInt();
	asm int timerInt;
}

void Parallelism::restoreSchedStack()
// Restore the scheduler stack
{
	for (int i = MaxStack - 1; i >= (MaxStack - 14); i--)
		contextSched.stack[i] = schedCopy[i];
}

int Parallelism::getCurrentTask()
{
	return currentTask;
}

void Parallelism::setCurrentTask(int taskNum)
// Sets the next task to be run
{
	if (taskNum <= totalTasks)
		currentTask = taskNum;
}

int Parallelism::getTotalTasks()
// Gets total tasks declared
{
	return totalTasks;
}

int Parallelism::getDeadlock()
{
	return deadlock;
}

void Parallelism::setDeadlock()
{
	deadlock = true;
}

int Parallelism::contextSwitchOn()
// flag which enables context switch
{
	if (endOfTimeSlice) //is current time slice finished ?
	{
		endOfTimeSlice = false;
		context[currentTask].contextSwitchFlag = true;
		callScheduler();	// return control to the scheduler
		return 1;
	}
	context[currentTask].contextSwitchFlag = true;
	return 0;
}

void Parallelism::contextSwitchOff()
// Disable context switch
{
	context[currentTask].contextSwitchFlag = false;
	//contextSwitchFlag = false;
}

int Parallelism::getContextSwitch()
{
	return context[currentTask].contextSwitchFlag;
}
int Parallelism::getContextSwitchFlag(int taskNum) {
	return SMARTS.context[taskNum].contextSwitchFlag;
}
void Parallelism::setProgInt()
// flag indicates to the extern function 'timerInterruptHandler' 
// that this is an internal SMARTS software interrupt call, 
// and the original BIOS function has no to be called.
{
	progInt = true;
}

void Parallelism::resetProgInt()
{
	progInt = false;
}

int Parallelism::getProgInt()
{
	return progInt;
}

void Parallelism::setEndOfTimeSlice()
// flag indicates that when 'context switch' will be enabled, 
// it must also return the control to the scheduler.
{
	endOfTimeSlice = true;
}

char Parallelism::getName(int taskNum)	// returns name found or ' ' if not
{
	return (taskNum <= totalTasks) ? context[taskNum].name : ' ';
}

char Parallelism::getCurrentName()
{
	return context[currentTask].name;
}

taskStatus Parallelism::getStatus(int taskNum)
// returns status or undefined if not found
{
	return (taskNum <= totalTasks) ? context[taskNum].status : UNDEFINED;
}

taskStatus Parallelism::getCurrentStatus()
{
	return context[currentTask].status;
}

void Parallelism::resume(int taskNum)
{
	if (taskNum < totalTasks)
		context[taskNum].status = READY;
}

void Parallelism::resume(char taskName)
{
	for (int i = 0; i < totalTasks; ++i)
		if (context[i].name == taskName)
			context[i].status = READY;
}


void Parallelism::setCurrentNotActive()
{
	context[currentTask].status = NOT_ACTIVE;
	--activeTasks;
}
void Parallelism::suspended()
{
	context[currentTask].status = SUSPENDED;
	callScheduler();
}

void Parallelism::incrPriority(int taskNum)
{
	if (taskNum < totalTasks)
		context[taskNum].incrPriority();
}
void Parallelism::setOriginalPriority(int taskNum)
{
	if (taskNum < totalTasks)
		context[taskNum].setOriginalPriority();
}

void Parallelism::setCurrentOriginalPriority()
{
	context[currentTask].setOriginalPriority();
}

Event *Parallelism::getExpectedEvent(int taskNum)
// returns *Event  or  NULL  if not found
{
	return (taskNum <= totalTasks) ? context[taskNum].expectedEvent : NULL;
}

Event *Parallelism::getCurrentExpectedEvent()
{
	return context[currentTask].expectedEvent;
}

void Parallelism::setCurrentExpectedEvent(Event *expectedEvent)
{
	context[currentTask].expectedEvent = expectedEvent;
}

void Parallelism::sleep(int t)
// Current task sleeps for 't' milliseconds
{
	if (t < MAXINT)
	{
		context[currentTask].sleepCount = t / 55 + 1;
		context[currentTask].status = SLEEP;
		++sleepTasks;
		callScheduler();		// return control to scheduler
	}
}

void Parallelism::sleepDecr(int taskNum)
{
	if (taskNum < totalTasks)
		context[taskNum].sleepDecr();
}

void Parallelism::getCurrentStack(unsigned &StackSeg, unsigned &StackPtr)
// Load current task stack pointer
{
	StackSeg = context[currentTask].stackSeg;
	StackPtr = context[currentTask].stackPtr;
}

void Parallelism::setCurrentStack(unsigned StackSeg, unsigned StackPtr)
// Save current task stack pointer
{
	context[currentTask].stackSeg = StackSeg;
	context[currentTask].stackPtr = StackPtr;
}

void Parallelism::getSchedStack(unsigned &StackSeg, unsigned &StackPtr)
// Load scheduler  stack pointer
{
	StackSeg = contextSched.stackSeg;
	StackPtr = contextSched.stackPtr;
}

void Parallelism::handleTimers()
// handling of the sleep status mode 
//------------ NEW ------  handling of the periodic tasks 
{
	for (int i = totalTasks - 1; i >= 0; --i)
	{
		if (getStatus(i) == SLEEP)
		{
			sleepDecr(i);
			if (getStatus(i) == READY)
				--sleepTasks;
		}

	}
	//---------------------NEW--------------------
	//handling of the periodic tasks 

	for (int j = totalTasks - 1; j >= 0; --j)
	{
		//decrease the deadlines for all tasks
		context[j].cycleTimeLeft--;

		//if the task finished his period cycle and still have more cycles to do
		if (getCycleTimeLeft(j) <= 0)
		{
			if (getNumOfCyclesLeft(j) > 0)
			{

				if (getStatus(j) != NOT_ACTIVE) //SUSPENDED OR SLEEP 
				{
					SMARTS.setError();
					cprintf("\nerror of task %d", j);//prints the number of the task that has error
					break;
				}
				context[j].redeclare();

			}

		}
	}

}

//--------------------------------- NEW -----------------------------	

void Parallelism::setError()
{
	error = 1;
}

int Parallelism::getError()
{
	return error;
}


//--------------------------------- NEW -----------------------------
int Parallelism::getCurrentPriority(int numOfTask) {          // get the current running task Priority
	return context[numOfTask].currentPriority;
}
void Parallelism::setCurrentPriority(int taskNum, int currPriority)
{
	context[taskNum].currentPriority = currPriority;
}
int Parallelism::getNumOfCyclesLeft(int i)                // ---------NEW 
{
	return context[i].numOfCyclesLeft;
}
int Parallelism::getCycleTime(int i)                 // ---------NEW 
{
	return context[i].cycleTime;
}
int Parallelism::getCycleTimeLeft(int i)          // ---------NEW
{
	return context[i].cycleTimeLeft;
}
//--------------------------------- NEW -----------------------------


void Parallelism::taskEnd()
// This function is called after the last operation of a task, instead of function return
{
	SMARTS.setCurrentNotActive();
	SMARTS.callScheduler();	// return the control to the scheduler to run a next task
}

/**********    class body:  Task     **********/
Task::Task()
{
	stack[MaxStack - 14] = _BP;
	stack[MaxStack - 13] = _DI;
	stack[MaxStack - 12] = _SI;
	stack[MaxStack - 11] = _DS;
	stack[MaxStack - 15] = _ES;
	stack[MaxStack - 9] = _DX;
	stack[MaxStack - 8] = _CX;
	stack[MaxStack - 7] = _BX;
	stack[MaxStack - 6] = _AX;
	stackSeg = FP_SEG(&stack[MaxStack - 14]);
	stackPtr = FP_OFF(&stack[MaxStack - 14]);
	status = NOT_ACTIVE;
	sleepCount = 0;
	currentPriority = priority = 0;
	contextSwitchFlag = true;
}
//-----------------------------------------------------
void Task::declare(void far *code, void far *userTaskEnd, char name, /*NEW*/int numOfCyclesLeft, int cycleTime /*NEW*/)
{
	stack[MaxStack - 5] = FP_OFF(code);
	stack[MaxStack - 4] = FP_SEG(code);
	stack[MaxStack - 3] = _FLAGS;
	stack[MaxStack - 2] = FP_OFF(userTaskEnd);
	stack[MaxStack - 1] = FP_SEG(userTaskEnd);
	this->name = name;
	this->numOfCyclesLeft = numOfCyclesLeft;
	this->currentPriority = priority = cycleTime;
	this->cycleTime = this->cycleTimeLeft = cycleTime;
	//--------- New  
//	save the origin stack  
	for (int i = MaxStack - 1; i >= (MaxStack - 14); i--)
		this->stackCopy[i] = this->stack[i];

	status = READY;
}

//-------------------- New --------------------
void Task::redeclare()
{
	for (int i = MaxStack - 1; i >= (MaxStack - 14); i--)
		this->stack[i] = this->stackCopy[i];

	cycleTimeLeft = cycleTime;
	numOfCyclesLeft--;

	stackSeg = FP_SEG(&stack[MaxStack - 14]);
	stackPtr = FP_OFF(&stack[MaxStack - 14]);
	SMARTS.activeTasks++;
	status = READY;

}


//----------------------------------------------------
void Task::incrPriority()
{
	--currentPriority;
}
//----------------------------------------------------
void Task::setOriginalPriority()
{
	currentPriority = priority;
}
//----------------------------------------------------
void Task::sleepDecr()
// Decrements the sleep counter and update the task status accordingly
{
	if (status == SLEEP)
	{
		if (sleepCount > 0)
			--sleepCount;
		if (!sleepCount)
			status = READY;
	}
}

PriorityQueue::PriorityQueue()
{ //initializes the queue with 0 (maxTask is the size of the queue
	numOfElements = 0;
	for (int i = 0; i < MaxTask; i++) {
		queue[i] = 0;
	}
}

int PriorityQueue::getNumOfElements()
{
	return numOfElements;
}

void PriorityQueue::enqueue(int element)
{
	int index = 0;
	int currPriority = SMARTS.getCurrentPriority(element);
	for (int i = numOfElements - 1; i > 0; i--)
	{
		if (SMARTS.getCurrentPriority(queue[i]) > currPriority)
		{//if the priority of the element is a smaller number than the on we stand on I need to stop looking
			index = i;
			break;
		}
	}

	for (int j = numOfElements - 1; j >= index; j--)
	{//from here on out I will move all the elements one cell left and then when I'll reach index I'll put the element there
		queue[j + 1] = queue[j];
	}
	queue[index] = element;
	numOfElements++;//there is one more element right now

}
int PriorityQueue::dequeue()
{
	int res = queue[0];
	for (int j = 0; j < numOfElements; j++)
	{
		queue[j] = queue[j + 1];

	}
	numOfElements--;
	return res;
}

int PriorityQueue::top()
{//get a peek of the first element
	return queue[0];
}

int PriorityQueue::isEmpty()
{//checks whether the queue is empty
	if (numOfElements <= 0)
		return 1;//true
	else
		return 0;//false
}

void PriorityQueue::printQueue()
{//to check if it works
	FILE *f;
	cout << "\n *************   Queue contains:    *********************\n";
	f = fopen("bonus.txt", "a");  //a  == append
	fprintf(f, "\n *************   Queue contains:    *********************\n");
	fclose(f);
	for (int i = 0; i < numOfElements; i++)
	{
		printf("%d \n", queue[i]);
		f = fopen("Outputname.txt", "a");  //a  == append
		fprintf(f, "%d\n", queue[i]);
		fclose(f);
	}
}

int PriorityQueue::findTaskQue(int task)
{
	for (int j = 0; j < numOfElements; j++)
	{
		if (queue[j] == task)
			return 1;
	}
	return 0;
}




Mutex::Mutex( )
{
	s = 1;
	Level = 0;
	Owner = -1;
	this->p = 1;
}

void Mutex::acquire()
{
	SMARTS.contextSwitchOff();//this part is atomic
	if (s == 1 || Owner == SMARTS.getCurrentTask())
		s = 0;//if the one that wants to aquire the mutex is the owner or the mutex is avaliable just take it
	else {

		int currentTask = SMARTS.getCurrentTask();
		pQueue.enqueue(currentTask);

		/*the owner inherits the priority of the current task that wishes to aquire the mutex*/
		if (this->p == 1)
		{
			int currPriority = SMARTS.getCurrentPriority(currentTask);
			if (currPriority < SMARTS.getCurrentPriority(Owner))
			{
				SMARTS.setCurrentPriority(Owner, currPriority);
			}
		}
				//the end of the priority inheritance.

		SMARTS.suspended();
	}
	Owner = SMARTS.getCurrentTask();
	Level++;

	SMARTS.contextSwitchOn();
}

void Mutex::release()
{
	SMARTS.contextSwitchOff();
	if (Owner == SMARTS.getCurrentTask())
		if (--Level)
			return;
		else {
			Owner = -1;
			if (!pQueue.isEmpty()) {
				/*restores the priority to res  */
				SMARTS.setCurrentOriginalPriority();

				int res = pQueue.dequeue();
				SMARTS.resume(res);
			}
			else
			{
				s = 1;
			}
		}
	SMARTS.contextSwitchOn();
}

PriorityMutexs::PriorityMutexs(int size)
{
	numOfMutexes = size;
	for (int i = 0; i < size; i++)
	{
		mutexArr[i] =  Mutex();
	}

}

void PriorityMutexs::release(int mutexIndex)
{
	SMARTS.contextSwitchOff();
	mutexArr[mutexIndex].release();
	SMARTS.contextSwitchOn();

}

void PriorityMutexs::acquire(int mutexIndex)
{
	SMARTS.contextSwitchOff();
	if (mutexArr[mutexIndex].s == 1 || mutexArr[mutexIndex].Owner == SMARTS.getCurrentTask())
		mutexArr[mutexIndex].s = 0;
	else {

		int currentTask = SMARTS.getCurrentTask();
		mutexArr[mutexIndex].pQueue.enqueue(currentTask);

		/*the owner inherits the priority of the current task that wishes to aquire the mutex*/
		if (mutexArr[mutexIndex].p == 1)
		{
				int currPriority = SMARTS.getCurrentPriority(currentTask);
				if (currPriority < SMARTS.getCurrentPriority(mutexArr[mutexIndex].Owner))
				{
					//inherites to all the chain of tasks
					SMARTS.setCurrentPriority(mutexArr[mutexIndex].Owner, currPriority);
					recursiveInheritence(mutexArr[mutexIndex].Owner);//put comment to make without recursive
				}                        
		}	
					//the end of the priority inheritance.

		SMARTS.suspended();
	}
	mutexArr[mutexIndex].Owner = SMARTS.getCurrentTask();
	mutexArr[mutexIndex].Level++;

	SMARTS.contextSwitchOn();
}
void PriorityMutexs::recursiveInheritence(int task)
{
	for (int i = 0; i < numOfMutexes; i++)
	{
		if (mutexArr[i].pQueue.findTaskQue(task))//task in the queue of mutex i 
		{
			int taskPriority = SMARTS.getCurrentPriority(task);
			if (taskPriority < SMARTS.getCurrentPriority(mutexArr[i].Owner))//if the task has higher priority the owner of the mutex will inherite the priority
			{
				SMARTS.setCurrentPriority(mutexArr[i].Owner, taskPriority);
				recursiveInheritence(mutexArr[i].Owner);//the recursive part
			}
		}
	}
}