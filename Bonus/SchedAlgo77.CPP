/********************************************************************/
/* A Small Real Time System for the Real-Time laboratory                 */
/* built by: A.Teitelbaum on an idea of H.G.Mendelbaum                  */
/* Jerusalem College of Technology, 5759-64 (1999)                            */
/* SchedAlgo64.CPP, short-term scheduler algorithms                */
/****************************************************************/

/*By:
 הודיה אלימלך 318627247
*/

#include "smarts77.h"

int roundRobin( )
// Round Robin short-term algorithm 
{
	int count;
	int nextTask = (SMARTS.getCurrentTask()+1)%SMARTS.getTotalTasks( );
	for (count=0;
	     SMARTS.getStatus(nextTask)!=READY && count<SMARTS.getTotalTasks( );
	     count++)
	     nextTask=++nextTask%SMARTS.getTotalTasks( );
	if (count == SMARTS.getTotalTasks( ))	// no found READY task to run
		nextTask = SMARTS.getTotalTasks( );
	return nextTask;
}


//----------------------NEW-----------------------------//
int EDF()
// Edf algorithm 
{
	int min = MAXINT;
    int nextTask = SMARTS.getTotalTasks();
	
	for (int i = 0; i < SMARTS.getTotalTasks(); i++)
	{
		if(SMARTS.getStatus(i) == READY)
		{
			//find the task with the soonest deadline
			if (SMARTS.getCycleTimeLeft(i) < min)
			{
				min = SMARTS.getCycleTimeLeft(i);
				nextTask = i;
			}

		}
	}
	return nextTask;
}
int RMS()
// RMS algorithm 
{
	int min = MAXINT;
	int nextTask = SMARTS.getTotalTasks();

	for (int i = 0; i < SMARTS.getTotalTasks(); i++)
	{
		if (SMARTS.getStatus(i) == READY)
		{
			//find the task with the soonest deadline
			if (SMARTS.getCurrentPriority(i) < min)
			{
				min = SMARTS.getCurrentPriority(i);
				nextTask = i;
			}

		}
	}
	return nextTask;
}

